#!/bin/bash

# check if build directory has been created
if [ ! -e build ] ; then
    mkdir build
fi
 
vagrant up --provision

if [ $? -eq 0 ] ; then
    echo "*** Deployed without errors ***"
    exit 0
else
    echo "*** Deployed with ERRORs.   ***" >&2
    exit 1
fi
