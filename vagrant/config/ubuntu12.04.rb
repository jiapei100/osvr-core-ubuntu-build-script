module MyVars
  OS        = "ubuntu/precise64"
  OS_URL    = "https://atlas.hashicorp.com/ubuntu/boxes/precise64"
  BUILD_DIR = "precise64"
end
