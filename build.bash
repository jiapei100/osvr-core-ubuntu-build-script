#!/bin/bash

# Command Line Arguments
# $1 - Set the path to the root project to build
# If $1 is unset, than use dir that was called

# Import files
## ==== Functions ==== ##

## Checks out the latest tag
function check_latest_tag {
  # Checkout the latest tag
  git fetch --tags

  # get the latest tag
  latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

  # get the latest tag
  git checkout $latestTag
  git submodule update --init --recursive
}

function check_latest_tag_only {
  # Checkout the latest tag
  git fetch --tags

  # get the latest tag
  latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

  # get the latest tag
  git checkout $latestTag
}

# gets the latest from git head
function get_head {
  git checkout master
  git submodule update --init --recursive
  git pull
}

function get_head_only {
  git checkout master
  git pull
}



function setup {
  if [ ! -e $INSTALL_DIR ] ; then
    mkdir $INSTALL_DIR
  fi

  if [ ! -e $SETUP_DIR ] ; then
    mkdir $SETUP_DIR
  fi

  # Adding cmake ppa because requires cmake 3.x 
  # Ubuntu 14.04 only has cmake 2.x
  sudo -E add-apt-repository ppa:george-edison55/cmake-3.x
  sudo -E add-apt-repository ppa:ubuntu-toolchain-r/test
  sudo -E apt-get update

  ## boost dependancies
  BOOST="libboost-dev libboost-thread-dev libboost-program-options-dev libboost-filesystem-dev"

  # The rest of the dependancies
  DEPS="cmake ${BOOST} libopencv-dev python2.7 libusb-1.0-0-dev gcc-5 g++-5 git-core libeigen3-dev markdown libmarkdown2-dev libsdl2-dev libglew-dev"
  sudo apt-get --yes --force-yes install $DEPS


  # libfunctionality
  git clone --recursive https://github.com/OSVR/libfunctionality.git
  if [ ! -e libfunctionality/build ] ; then
    mkdir libfunctionality/build
  fi

  # jsoncpp
  git clone --recursive https://github.com/VRPN/jsoncpp
  if [ ! -e jsoncpp/build ] ; then
    mkdir jsoncpp/build
  fi

  # render manager
  git clone https://github.com/sensics/OSVR-RenderManager.git
  if [ ! -e OSVR-RenderManager/build ] ; then
    mkdir OSVR-RenderManager/build
  fi


  # building osvr-core
  git clone --recursive https://github.com/OSVR/OSVR-Core.git
  #git clone --depth=50 --branch=appveyor https://github.com/OSVR/OSVR-Core.git OSVR/OSVR-Core
  git submodule update --init --recursive

  if [ ! -e OSVR-Core/build ] ; then
    mkdir OSVR-Core/build
  fi

  # OpenSceneGraph
  open_scene_graph_setup

  # viewer
  tracker_viewer_setup

  echo > $SETUP_DIR/setup.done
}

function build_libfunctionality {
  pushd .
  cd libfunctionality
  if [ $LATEST_TAG == true ] ; then
      get_head
  else
      check_latest_tag
  fi
  cd build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make all install
  popd
}

function build_jsoncpp {
  pushd .
  cd jsoncpp
  get_head
  git pull
  cd build
  cmake .. -DJSONCPP_WITH_CMAKE_PACKAGE=ON -DJSONCPP_LIB_BUILD_SHARED=OFF -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
  make install
  popd
}

function build_rendermanager {
  pushd .
  cd $RENDER_MANAGER_DIR
  #get_head_only
  #check_latest_tag_only
  git submodule init vendor/vrpn
  git submodule update
  git pull
  cd build
  #cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make install
  popd
}

function build_osvr {
  pushd .
  cd OSVR-Core
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}

function clean {
  pushd .
  cd $1/build
  make clean
  popd
}

function open_scene_graph_setup {
  git clone --recursive https://github.com/openscenegraph/OpenSceneGraph
  if [ ! -e OpenSceneGraph/build ] ; then
      mkdir OpenSceneGraph/build
  fi
}

function build_open_scene_graph {
  pushd .
  cd OpenSceneGraph
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ..
  make install
  popd
  # copy all lib64 into lib
  cp -r dist/lib64/* dist/lib/.
}

function tracker_viewer_setup {
  git clone --recursive https://github.com/OSVR/OSVR-Tracker-Viewer
  if [ ! -e OSVR-Tracker-Viewer/build ] ; then
    mkdir OSVR-Tracker-Viewer/build
  fi
}

function build_tracker_viewer {
  pushd .
  cd OSVR-Tracker-Viewer
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make install
  popd
}

## ==== Additional Plugins ==== ##
function steamvr_setup {
  # SteamVR-OSVR
  git clone --recursive https://github.com/OSVR/SteamVR-OSVR
  if [ ! -e SteamVR-OSVR/build ] ; then
    mkdir SteamVR-OSVR/build
  fi
  pushd .
  cd SteamVR-OSVR
  git submodule update --init --recursive

  ### need to update cmake
  echo 'set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpic")' >> CMakeLists.txt
  popd

  echo > $SETUP_DIR/steamvr_plugin.done
}

function build_steam {
  pushd .
  cd SteamVR-OSVR
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  #CMAKE_DIR=`(ls /usr/share | grep cmake)`
  #cmake .. -DJSONCPP_WITH_CMAKE_PACKAGE=ON -DJSONCPP_LIB_BUILD_SHARED=OFF -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=ON -DCMAKE_CXX_FLAGS=-fPIC ..
  #cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS_ON..
  #cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=ON ..
  #cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=ON -DCMAKE_INSTALL_LIBDIR=lib ..
  make install
  popd
}

## ==== Additional Applications ==== ##
function osvr_config_setup {
  # SteamVR-OSVR
  git clone --recursive https://github.com/OSVR/OSVR-Config
  if [ ! -e OSVR-Config/build ] ; then
    mkdir OSVR-Config/build
  fi

  ## Shoot yourself in the face, installing .net :(

  ### if ubuntu 14.04
  sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ trusty main" > /etc/apt/sources.list.d/dotnetdev.list'
  sudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
  sudo apt-get update

  ### if ubuntu 16.04
  #sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
  #sudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
  #sudo apt-get update

  sudo apt-get install -y dotnet-dev-1.0.0-preview2-003121
  # update to latest npm
  curl -sL https://deb.nodesource.com/setup | sudo bash -
  sudo apt-get install -y nodejs
  sudo npm update -g npm
  sudo npm install -g bower 
  sudo npm install -g gulp 

  echo > $SETUP_DIR/osvr_config.done
}

function osvr_video_status_setup {
  git clone https://github.com/sensics/OSVR-HDK-Video-Status
  if [ ! -e OSVR-HDK-Video-Status/build ] ; then
    mkdir OSVR-HDK-Video-Status/build
  fi
  echo > $SETUP_DIR/osvr_video_status.done
}

function build_video_status {
  pushd .
  cd OSVR-HDK-Video-Status
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}

function vrpn_setup {
  git clone https://github.com/vrpn/vrpn
  if [ ! -e vrpn/build ] ; then
    mkdir vrpn/build
  fi
  echo > $SETUP_DIR/vrpn.done
}

function build_vrpn {
  pushd .
  cd vrpn
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}

# sets up the open vr fork from haag
function openvr_setup {
    git clone https://github.com/ChristophHaag/openvr.git
    if [ ! -e openvr/build ] ; then
      mkdir openvr/build
    fi
    echo > $SETUP_DIR/openvr.done
}

function build_openvr {
  pushd .
  cd openvr
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCAME_BUILD_TYPE=Release $ROOT_DIR/openvr
  #cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}



## === Argument Handeling === ##
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

ROOT_DIR=$(pwd)
build_steam=false
use_help=false
no_clean=false
LATEST_TAG=false

while getopts "d:shrl" opt; do
    case "$opt" in
    d)  
        ROOT_DIR=$OPTARG
        ;;  
    s)  build_steam=true
        ;; 
    r)  no_clean=true
        ;; 
    l)  LATEST_TAG=true
        ;; 
    h)  use_help=true
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ $use_help == true ] ; then
  echo 'Usage: build [-d <path to root directory>] [-s]'
  echo '-d the root directory to install (optional)'
  echo '-s to build SteamVR-OSVR Plugin'
  echo '-h to view help'
  echo '-r to rebuild not using clean'
  exit 0;
fi


## set the root directory for building
pushd .
cd $ROOT_DIR
## ====================== ##


## == Global Variables == ##
INSTALL_DIR=$(pwd)/dist
SETUP_DIR=$INSTALL_DIR/setup
LIBFUNCTIONALITY_DIR=$(pwd)/libfunctionality
JSONCPP_DIR=$(pwd)/jsoncpp
RENDER_MANAGER_DIR=$(pwd)/OSVR-RenderManager

## Make sure using g++-5
export CXX="g++-5" CC="gcc-5"
## ====================== ##

  #build_rendermanager
  #build_open_scene_graph 
  #build_tracker_viewer
  #steamvr_setup
  #openvr_setup
  #build_steam
  #build_openvr

  #osvr_config_setup

  #osvr_video_status_setup
  #build_video_status
  #vrpn_setup
  #build_vrpn
  #exit 1

# check if setup has been completed
if [ ! -e $SETUP_DIR/setup.done ] ; then
  setup
fi

if [ $build_steam == true ] ; then
  if [ ! -e $SETUP_DIR/steamvr_plugin.done ] ; then
    steamvr_setup
    openvr_setup
  fi
  if [ ! -e $SETUP_DIR/openvr.done ] ; then
    openvr_setup
  fi
fi

# clean all
if [ $no_clean == false ] ; then
  clean libfunctionality
  clean jsoncpp
  clean OSVR-Core
  if [ $build_steam == true ] ; then
    clean SteamVR-OSVR
    clean openvr
  fi
fi

# build all
build_libfunctionality
build_jsoncpp
build_osvr
build_rendermanager
if [ $build_steam == true ] ; then
  echo '============ building steam'
  build_steam
  build_openvr
fi


# exit install dir
popd
