#!/bin/bash

# A library for compiling the OSVR core

## ==== Functions ==== ##
function steamvr_setup {
  # SteamVR-OSVR
  git clone --recursive https://github.com/OSVR/SteamVR-OSVR
  if [ ! -e SteamVR-OSVR/build ] ; then
    mkdir StreamVR-OSVR/build
  fi
  pushd .
  cd SteamVR-OSVR
  git submodule update --init --recursive
  popd

  echo > steamvr_plugin_setup.done
}

function build_steam {
  pushd .
  cd SteamVR-OSVR/build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}
