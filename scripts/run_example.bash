#!/bin/bash

INSTALL_DIR=$(pwd)/dist
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

pushd .
cd $INSTALL_DIR/bin
#./RenderManagerOpenGLCoreExample
#./RenderManagerOpenGLPresentExample
#./osvr_reset_yaw
./RenderManagerOpenGLSharedContextPresentExample


popd
